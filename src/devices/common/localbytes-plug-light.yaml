packages:
  base: !include base.yaml

esp8266:
  board: esp01_1m

binary_sensor:
  - platform: gpio
    pin:
      number: GPIO3
      mode: INPUT_PULLUP
      inverted: true
    name: button
    id: button_1
    disabled_by_default: true
    on_click:
      - min_length: !secret primary_button_duration_min
        max_length: !secret primary_button_duration_max
        then:
          - light.toggle: switch_1
      - min_length: !secret reset_button_duration_min
        max_length: !secret reset_button_duration_max
        then:
          - button.press: factory_reset_1

output:
  # Relay State LED
  - platform: esp8266_pwm
    id: state_led
    pin:
      number: GPIO13
      inverted: true
  - platform: gpio
    pin: GPIO14
    id: relay
light:
  - platform: binary
    output: state_led
    id: led
  - platform: binary
    name: "switch"
    id: switch_1
    output: relay
    restore_mode: RESTORE_DEFAULT_OFF

sensor:
  - platform: hlw8012
    sel_pin:
      number: GPIO12
      inverted: true
    cf_pin: GPIO4
    cf1_pin: GPIO5
    change_mode_every: 3
    update_interval: 6s
    current:
      name: "current"
      id: current
      unit_of_measurement: A
      accuracy_decimals: 3
      filters:
        - lambda: return x * id(current_multiply);
    voltage:
      name: "voltage"
      id: voltage
      unit_of_measurement: V
      accuracy_decimals: 1
      filters:
        - lambda: return x * id(voltage_multiply);
    power:
      name: "power"
      id: power
      unit_of_measurement: W
      accuracy_decimals: 0
      filters:
        - lambda: return x * id(power_multiply);

# Make calibration factor data readable/setable from home assistant
api:
  services:
    - service: calibrate_voltage
      variables:
        actual_value: float
      then:
        - lambda: |-
            id(voltage_multiply) = actual_value / id(voltage).raw_state;
        - number.set:
            id: voltage_factor
            value: !lambda "return id(voltage_multiply);"

    - service: calibrate_power
      variables:
        actual_value: float
      then:
        - lambda: |-
            id(power_multiply) = actual_value / id(power).raw_state;
        - number.set:
            id: power_factor
            value: !lambda "return id(power_multiply);"

    - service: calibrate_current
      variables:
        actual_value: float
      then:
        - lambda: |-
            id(current_multiply) = actual_value / id(current).raw_state;
        - number.set:
            id: current_factor
            value: !lambda "return id(current_multiply);"

globals:
  - id: voltage_multiply
    type: float
    restore_value: true
    initial_value: "0.3"

  - id: power_multiply
    type: float
    restore_value: true
    initial_value: "0.133"

  - id: current_multiply
    type: float
    restore_value: true
    initial_value: "0.805"

number:
  - platform: template
    name: "Voltage Calibration Factor"
    id: voltage_factor
    icon: "mdi:sine-wave"
    min_value: 0
    max_value: 10
    step: 0.001
    entity_category: diagnostic
    mode: box
    lambda: |-
      return id(voltage_multiply);
    set_action:
      lambda: |-
        id(voltage_multiply) = x;

  - platform: template
    name: "Power Calibration Factor"
    id: power_factor
    icon: "mdi:flash"
    min_value: 0
    max_value: 10
    step: 0.001
    entity_category: diagnostic
    mode: box
    lambda: |-
      return id(power_multiply);
    set_action:
      lambda: |-
        id(power_multiply) = x;

  - platform: template
    name: "Current Calibration Factor"
    id: current_factor
    icon: "mdi:current-ac"
    min_value: 0
    max_value: 10
    step: 0.001
    entity_category: diagnostic
    mode: box
    lambda: |-
      return id(current_multiply);
    set_action:
      lambda: |-
        id(current_multiply) = x;
