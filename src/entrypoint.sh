#!/bin/bash

TERM=xterm-256color

option_compile_single="Compile a single device"
option_upload_single="Update a single device"
option_upload_all="Update all devices"
option_show_dashboard="Show dashboard"
option_interactive_shell="Enter interactive ESPHome shell"

task="$(gum choose --header "What would you like to do?" "$option_compile_single" "$option_upload_single" "$option_upload_all" "$option_show_dashboard" "$option_interactive_shell")"

execute_command() {
  echo "$1" | gum format
  eval "$1"
}

all_files="$(find devices -maxdepth 1 -name "*.yaml" | sort | tr '\n' ' ')"

if [ "$task" == "$option_compile_single" ] || [ "$task" == "$option_upload_single" ]; then
  file="$(gum choose --header "Choose file" $all_files)"
  execute_command "esphome compile ${file}"

  if [ "$task" == "$option_upload_single" ]; then
    execute_command "esphome upload ${file}"
  fi

elif [ "$task" == "$option_upload_all" ]; then
  for file in $all_files; do
    execute_command "esphome compile ${file}" || true
    execute_command "esphome upload ${file}" || true
  done

elif [ "$task" == "$option_show_dashboard" ]; then
  esphome dashboard devices/

elif [ "$task" == "$option_interactive_shell" ]; then
  echo "_Starting interactive shell…_" | gum format
  # while true; do
  #   input="$(gum input --prompt "> esphome " --placeholder="")"
  #   [ "$input" == "exit" ] && break
  #   eval esphome $input
  # done
  bash
fi
