# Home automation v2

A second attempt at home-automation, using ESPHome for sensors and controllers.

> [Home automation v1](https://gitlab.com/danhawkes/home-automation-v1) took Nexa-brand plug sockets and controlled them using a Raspberry Pi.
>
> In hindsight this required lot of wheel re-invention and was limited by the capabilities of the hardware.

## Project structure

```
├── docker-compose.yaml
├── Dockerfile
├── docs
├── README.md
└── src
   ├── devices
   │  ├── bed_duvet_heater.yaml
   │  ├── …
   │  ├── common
   │  │  ├── athom-smart-plug-v2-light.yaml
   │  │  └── …
   └── entrypoint.sh
```

## Bootstrapping devices

Instructions on how to bootstrap a device ready for flashing.

- [Smart Life TP24](./docs/smart-life.md)
- [Sonoff Mini](./docs/sonoff-mini.md)
- [Smart POW R2](./docs/sonoff-pow-r2.md)

## Running ESPHome tasks

ESPHome tasks are run in a container, because python packaging still sucks.

```
docker-compose run esphome
```

…then follow the prompts.
