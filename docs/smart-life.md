# Smart-Life device setup

- Boot into Kali (tuya-convert scripts are dangerous and change a load of stuff as root!)
- Clone tuya-convert:

  ```sh
  git clone https://github.com/ct-Open-Source/tuya-convert.git
  cd tuya-convert
  ```

- Run the install script (no need to modify)
- Connect to device from phone, enter wifi settings and allow to reboot.

From here, device can be managed from from regular linux via `setup.sh`.
