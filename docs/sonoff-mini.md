# Sonoff Mini device setup

- Download firmware and calculate hash
  ```sh
  wget http://thehackbox.org/tasmota/tasmota-wifiman.bin
  shasum -a 256 tasmota-wifiman.bin
  ```
- Start access point
  ```sh
  nmcli dev wifi hotspot ifname wlp2s0 ssid sonoffDiy password 20170618sn
  ```
- Find device ID, IP
  ```sh
  avahi-browse -t _ewelink._tcp --resolve
  ```
- Setup wifi credentials
  ```sh
  curl http://<device_ip>:8081/zeroconf/wifi -X POST --data '{"deviceId": "<device_id>","data":{"ssid": "<ssid>", "password": "<password>"}}'
  ```
- Disconnect from AP
- Start server with firmware (`serve`)
- OTA unlock and flash new firmware
  ```sh
  curl http://<device_ip_2>:8081/zeroconf/ota_unlock -XPOST --data '{"deviceid":"<device_id>","data":{} }'
  curl http://<device_ip_2>:8081/zeroconf/ota_flash -XPOST --data '{"deviceid":"<device_id>", "data":{"downloadUrl": "http://192.168.1.88:5000/tasmota-wifiman.bin", "sha256sum": "<firmware_hash>"} }'
  ```

## Troubleshooting

If device does not appear on wifi after flashing, try powering it off for a minute, then restarting.
