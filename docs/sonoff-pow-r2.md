# Sonoff Pow R2 Pinout

| Device | 1   | 2   | 3   | 4   | 5   | 6   |
| ------ | --- | --- | --- | --- | --- | --- |
| Sonoff | 105 | 104 | GND | TX  | RX  | VDD |
| FT232R | DTR | RX  | TX  | VCC | CTS | GND |

| Sonoff  | FT232R  |
| ------- | ------- |
| 105     | DTR     |
| 104     | **RX**  |
| **GND** | **TX**  |
| **TX**  | **VCC** |
| **RX**  | CTS     |
| **VDD** | **GND** |
